    // Variables globales
var globalResult = 0,                                   // Resultado de todas las operaciones
    lastActionWasOperand = false,                       // Booleano que indica si el ultimo boton pulsado era de operacion
    shouldEmptyResult = true,                           // Booleano que indicara si se debe vaciar el input result al escribir
    lastOperant,                                        // Variable en la que se guardara la ultima operacion
    lastResult = 0,                                     // Variable en la que se guardara el último resultado
    inpResult = document.getElementById("result"),      // Acceso rapido al input "result"
    inpOperant = document.getElementById("operant");    // Acceso rapido al input "operant"

function printNumber(ev) {
   
    if(shouldEmptyResult) {
        inpResult.value = ev.srcElement.innerText; //devuelve el componente del html que a lanzado ese evento y sacamos el innertext del boton
    }
    else {
        inpResult.value += ev.srcElement.innerText;
    }
    shouldEmptyResult = false;
    lastActionWasOperand = false;
}

function operate(ev) {

    switch (ev.srcElement.innerText) {
        // En el caso de cualquiera de las operaciones aritméticas aplicamos las mismas pautas
        case "+": case "-": case "/": case "*":
            if(lastActionWasOperand) {
                // Si el último botón pulsado era de operación se quita el que habia y se concatena el nuevo aal input "operant"
                inpOperant.value = inpOperant.value.substr(0,inpOperant.value.length-1) + ev.srcElement.innerText;
            }
            else {
                // Si lo último pulsado no era una operación se añade al input "operant" la nueva cifra introducida y la nueva operacion 
                inpOperant.value += inpResult.value + ev.srcElement.innerText;
                lastResult = inpResult.value;
                applyEqual();
            }
            lastActionWasOperand = true;
            lastOperant = ev.srcElement.innerText;
            break;
        case "C":
            globalResult = 0;
            lastActionWasOperand = false;
            shouldEmptyResult = true; 
            lastOperant = undefined;
            lastResult = 0;
            inpOperant.value = "";
            inpResult.value = "0";
            break;
        case "CE":
            inpResult.value = "0";
            shouldEmptyResult = true;
            break;
        case "←":
            if(inpResult.value.length == 1) {
                inpResult.value = "0";
                shouldEmptyResult = true;
            }
            else
            {
                inpResult.value = inpResult.value.substr(0, inpResult.value.length-1);
            }
            break;
        case "=":
            // Solo si se ha realizado alguna operación se ejecutará el calcular resultado
            if(lastOperant != undefined) {
                inpOperant.value = "";
                applyEqual();
            }
            break;
        case ".":
            // Solo añadimos decimales si no hay ya
            if(inpResult.value.indexOf(".") == -1) {
                inpResult.value += ".";
                shouldEmptyResult = false;
            }
            break;
    }

    // Funcion que aplica la ultima operacion pendiente y muestra el resultado en el input "result"
    function applyEqual() {
        if(lastOperant != undefined) {
            // En caso de no ser la primera primera operacion pulsada se calcula el resultado mediante eval
            // La funcion eval ejecuta un trozo de codigo en forma de string
            // Ej: Si globalResult = 50, lastOperant = "+" y lastResult = 77
            //     lo que el eval haria seria: eval("50-77")
            inpResult.value = eval(globalResult + lastOperant + lastResult);
        }
        globalResult = parseFloat(inpResult.value);
        shouldEmptyResult = true;
    }
}